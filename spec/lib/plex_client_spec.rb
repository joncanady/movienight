require 'rspec'
require_relative '../../lib/plex_client'

RSpec.describe PlexClient do
  subject { PlexClient.new }

  it "lists movies" do
    expect(subject.movie_titles).to include("Good Will Hunting")
  end
end
